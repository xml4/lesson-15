package com.feeko256.task15

import androidx.annotation.WorkerThread
import kotlinx.coroutines.flow.Flow

class ContactRepository(private val dao: Dao) {
    val allContacts: Flow<List<Contact>> = dao.getContacts()

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insert(contact: Contact) {
        dao.insertContact(contact)
    }
    suspend fun resetDb() {
        dao.resetDb()
    }
    suspend fun updateRow(rowId: Int?, newValue: String) {
        dao.updateRowValue(rowId, newValue)
    }
}