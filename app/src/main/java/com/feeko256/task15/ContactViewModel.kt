package com.feeko256.task15

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

class ContactViewModel(private val repository: ContactRepository) : ViewModel() {
    private val allContact: MutableStateFlow<List<Contact>> = MutableStateFlow(emptyList())
    val contacts: StateFlow<List<Contact>> get() = allContact
    private var job: Job? = null
    fun observeContacts() {
        job = viewModelScope.launch(Dispatchers.IO) {
            repository.allContacts.collect() { list ->
                allContact.update { list }
            }
            Log.e("AAA", "ASF")
        }
    }

    fun updateRowValue(rowId: Int?, newValue: String) {
        viewModelScope.launch {
            repository.updateRow(rowId, newValue)
        }
    }

    fun stopObserve() {
        job?.cancel()
    }

    fun insert(contact: Contact) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.insert(contact)
        }
    }

    fun resetDataBase() {
        viewModelScope.launch(Dispatchers.IO) {
            repository.resetDb()
        }
    }
}

