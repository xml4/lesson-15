package com.feeko256.task15

import android.app.Application

class ContactsApplication : Application() {

    private val database by lazy { MainDb.getDb(this) }
    val repository by lazy { ContactRepository(database.getDao()) }

}