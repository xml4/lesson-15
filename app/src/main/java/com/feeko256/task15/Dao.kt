package com.feeko256.task15

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow

@Dao
interface Dao {
    @Insert
    fun insertContact(item: Contact)
    @Query("SELECT * FROM contacts ORDER BY id")
    fun getContacts(): Flow<List<Contact>>
    @Query("DELETE FROM contacts")
    fun resetDb()

    @Query("UPDATE CONTACTS SET note = :newValue WHERE id = :rowId")
    suspend fun updateRowValue(rowId: kotlin.Int?, newValue: String)
}