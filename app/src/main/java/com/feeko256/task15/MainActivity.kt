package com.feeko256.task15

import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.ContactsContract
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.feeko256.task15.databinding.ActivityMainBinding
import com.feeko256.task15.recyclerViewFolder.ContactAdapter
import com.feeko256.task15.recyclerViewFolder.ContactListListener
import kotlinx.coroutines.launch


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val adapter = ContactAdapter(ContactListListener { contact ->
        binding.etNote.hint = "Заметка для ${contact.name}"
        binding.etNote.setText(contact.note)
        contactId = contact.id
    })
    private lateinit var viewModel: ContactViewModel
    private var contactId: Int? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        viewModel = ViewModelProvider(
            this,
            ContactViewModelFactory(repository = (applicationContext as ContactsApplication).repository)
        ).get(ContactViewModel::class.java)

        binding.rcView.layoutManager = LinearLayoutManager(this)
        binding.rcView.adapter = adapter

        lifecycleScope.launch {
            viewModel.contacts.collect() {
                adapter.updateContact(it)
            }
        }

        checkPermissions()
        binding.btSetNote.setOnClickListener {
            if (contactId!! > 0) {
                Log.i("EEE", contactId.toString())
                viewModel.updateRowValue(contactId, binding.etNote.text.toString())
                adapter.updateItem(contactId!!, binding.etNote.text.toString())
            }
        }
    }

    override fun onStart() {
        super.onStart()

        viewModel.observeContacts()
    }

    override fun onStop() {
        viewModel.stopObserve()
        super.onStop()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            1 -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    viewModel.resetDataBase()
                    getContact()
                }
            }
        }
    }

    private fun checkPermissions() {
        if (ContextCompat.checkSelfPermission(
                this,
                android.Manifest.permission.READ_CONTACTS
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            acceptPermissions()
        }
    }

    private fun acceptPermissions() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(android.Manifest.permission.READ_CONTACTS),
            1
        )
    }

    @SuppressLint("Range")
    private fun getContact() {
        Log.i("AAA", "ASDAS")
        val cursor =
            contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null)
        if (cursor != null && cursor.moveToLast()) {
            do {
                val name =
                    cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))
                val contactId =
                    cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID))
                val phoneCursor = contentResolver.query(
                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                    null,
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                    arrayOf(contactId),
                    null
                )
                if (phoneCursor?.moveToNext() == true) {
                    val phone =
                        phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                    viewModel.insert(Contact(name = name, number = phone.toString(), id = 0))
                }
                phoneCursor?.close()
            } while (cursor.moveToPrevious())
            cursor.close()
        }
    }
}