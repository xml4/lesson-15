package com.feeko256.task15.recyclerViewFolder

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ExpandableListView.OnChildClickListener
import android.widget.Toast
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.feeko256.task15.Contact
import com.feeko256.task15.R
import com.feeko256.task15.databinding.ContactItemBinding

class ContactAdapter(private val contactClick: ContactListListener) : RecyclerView.Adapter<ContactAdapter.ContactHolder>() {

    private lateinit var binding: ContactItemBinding
    var contactsList: MutableList<Contact> = mutableListOf()



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.contact_item, parent, false)
        binding = ContactItemBinding.bind(view)
        return ContactHolder(view, binding)
    }

    override fun getItemCount(): Int {
        return contactsList.size
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onBindViewHolder(holder: ContactHolder, position: Int) {
        holder.bind(contactsList[position], clickListener = contactClick)
        val item = contactsList[position]

    }
    @SuppressLint("NotifyDataSetChanged")
    fun updateItem(index: Int, value: String){
        contactsList[index-1].note = value
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun updateContact(contact: List<Contact>){
        contactsList.addAll(contact)
        notifyDataSetChanged()
    }
   @SuppressLint("NotifyDataSetChanged")
   class ContactHolder(item: View, private val binding: ContactItemBinding) : RecyclerView.ViewHolder(item) {
        fun bind(contact: Contact, clickListener: ContactListListener){
            binding.apply {
                val text: String;
                this.card.setOnClickListener{
                    clickListener.onCLick(contact)
                }
                if(contact.note == "" || contact.note == null){
                    text = "Id: ${contact.id}\nName: ${contact.name}\nNumber: ${contact.number}"
                }
                else{
                    text = "Id: ${contact.id}\nName: ${contact.name}\nNumber: ${contact.number}\n" +
                            "note: ${contact.note}"
                }

                tvTitle.text = text
            }
        }
    }
}
class ContactListListener(private val onItemClick: (contact: Contact) -> Unit, ){
    fun onCLick(contact: Contact){
        onItemClick(contact)
    }
}